"use strict";

function generateImages() {
  for (var e = config.get("bad-beats"), t = [], a = 0; a < 1; a++) {
    var o = sharp(tempPath),
        r = textPath + e[a] + ".svg";o = o.overlayWith(r, { left: config.get("template.text.left"), top: config.get("template.text.top") }), o = o.png();var s = new cards(),
        n = [],
        g = [];for (var c in s.deck) {
      for (var f in s.deck[c]) {
        n.push([f, c]);
      }
    }var i = 0;for (var l in n) {
      var f = n[l];i++;var a = 0;for (var p in n) {
        var h = n[p];if (a < i) a++;else {
          var v = [f, h];console.log(v), g.push(v), console.log("Combo: " + f[0] + " of " + f[1] + " and " + h[0] + " of " + h[1]);
        }
      }
    }console.log(g), console.log("Finished, number of combos: " + g.length);
  }
}var fs = require("fs"),
    config = require("config"),
    sharp = require("sharp"),
    cards = require("./classes/cards");var tempPath = config.get("template.path"),
    textPath = config.get("template.text.path"),
    cardsPath = config.get("template.cards.path");console.log(cards), generateImages();