'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Libraries
 **/
var fs = require('fs');
var config = require('config');
var sharp = require('sharp');
var cards = require('./classes/cards');

var ImageProcessor = function () {
	function ImageProcessor() {
		_classCallCheck(this, ImageProcessor);

		this.combinations = null;
		this.tempPath = config.get('template.path');
		this.textPath = config.get('template.text.path');
		this.cardsPath = config.get('template.cards.path');

		// Generate all possible combos
		this.generateCombinations();

		// Generate all images based off this
		this.generateImages();
	}

	_createClass(ImageProcessor, [{
		key: 'generateImages',
		value: function generateImages() {}
	}, {
		key: 'generateCombinations',
		value: function generateCombinations() {
			this.combinations = [];
			var badBeats = config.get('bad-beats');

			// Loop over each bad beat and generate all combinations of cards for each
			//for( var i = 0; i < badBeats.length; i++ ){
			for (var i = 0; i < 1; i++) {
				// Create the template image for this particular bad beat
				var bbTemplate = sharp(this.tempPath);

				// Overlay bad beat
				var bbTextPath = this.textPath + badBeats[i] + '.svg';
				bbTemplate = bbTemplate.overlayWith(bbTextPath, {
					left: config.get('template.text.left'),
					top: config.get('template.text.top')
				});

				bbTemplate = bbTemplate.png();

				// Loop over every combo of card and plant those in to this template
				var cardDeck = new cards();

				// Use all cards in hearts as example, generate all combos
				var cardList = [];

				// Create an easy list
				for (var key in cardDeck.deck) {
					// Card 1
					for (var cardOne in cardDeck.deck[key]) {
						cardList.push([cardOne, key]);
					}
				}

				// Find combos
				var skip = 0;
				for (var cardOneI in cardList) {
					var cardOne = cardList[cardOneI];
					skip++;

					var i = 0;
					for (var cardTwoI in cardList) {
						var cardTwo = cardList[cardTwoI];

						if (i < skip) {
							i++;
							continue;
						} else {
							var newCombo = [cardOne, cardTwo];
							this.combinations.push(newCombo);
						}
					}
				}
			}

			console.log(this.combinations);
		}
	}]);

	return ImageProcessor;
}();

exports.default = ImageProcessor;