'use strict';

var extend = require('extend');

function CardDeck() {
	/**
  * Variables
  **/
	var self = this;

	// Cards
	this.deck = {};

	// Settings
	this.settings = {
		"suits": ['heart', 'diamond', 'club', 'spade'],
		"cards": { "ace": false,
			"two": false,
			"three": false,
			"four": false,
			"five": false,
			"six": false,
			"seven": false,
			"eight": false,
			"nine": false,
			"ten": false,
			"jack": false,
			"queen": false,
			"king": false
		}
	};

	/**
  * Construct
  **/
	this._construct = function () {
		// Create cards
		for (var iz = 0; iz < this.settings.suits.length; iz++) {
			suitName = self.settings.suits[iz];
			self.deck[suitName] = extend(true, {}, self.settings.cards);
		}
	};

	/**
  * Return Card to deck
  **/
	this.returnCard = function (suit, card) {
		self.deck[suit][card] = false;
	};

	/**
  * Use card and remove from deck
  **/
	this.useCard = function (suit, card, position) {
		self.deck[suit][card] = position;
	};

	this._construct();
}

module.exports = CardDeck;