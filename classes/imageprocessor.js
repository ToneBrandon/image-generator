const fs = require('fs');
const config = require('config');
const sharp = require('sharp');
const cards = require('./cards');

//sharp.cache(false);
sharp.concurrency(4);

module.exports = class ImageProcessor {

	constructor() {
		this.combinations = null;
		this.tempPath = config.get('template.path');
		this.nbTempPath = config.get('template.path-nobeat');
		this.textPath = config.get('template.text.path');
		this.cardPath = config.get('template.cards.path');
		this.i = 0;
		this.ii = 0;
		this.iii = 0;
		this.iiii = 0;
		this.procCounter = 0;
		this.procCounterTwo = 0;

		this.globalDebugCount = 0;

		// Generate all possible combos
		this.generateCombinations();
	}

	generateCombinations() {
		this.combinations = [];
		this.sharpTemplates = [];

		var badBeats = config.get('bad-beats');

		// Loop over each bad beat and generate all combinations of cards for each
		//for( var i = 0; i < badBeats.length; i++ ){
		for( var i = 0; i < badBeats.length; i++ ){
			// Create the template image for this particular bad beat
			if( badBeats[i] == '' ){
				// Generic
				var bbTemplate = sharp(this.nbTempPath);
			}
			else {
				var bbTemplate = sharp(this.tempPath);

				// Overlay bad beat
				var bbTextPath = this.textPath + badBeats[i] + '.png';
				bbTemplate = bbTemplate.overlayWith(bbTextPath, {
					left: config.get('template.text.left'),
					top: config.get('template.text.top')
				});
			}


			// Loop over every combo of card and plant those in to this template
			var cardDeck = new cards;

			// Use all cards in hearts as example, generate all combos
			var cardList = [];

			// Create an easy list
			for( var key in cardDeck.deck ){
				// Card 1
				for( var cardOne in cardDeck.deck[key] ){
					cardList.push([cardOne, key]);
				}
			}

			// Find combos
			var skip = 0;
			for( var cardOneI in cardList ){
				var cardOne = cardList[cardOneI];

				var iz = 0;
				for( var cardTwoI in cardList ){
					var cardTwo = cardList[cardTwoI];

				
						var newCombo = [cardOne, cardTwo];
						this.combinations.push(newCombo);

						if( badBeats[i] == '' ) 
							var beatName = 'Generic';
						else
							var beatName = badBeats[i];

						console.log(beatName);

						this.generateImage(bbTemplate.clone(), beatName, newCombo);
				}
			}
		}

		return this.combinations;
	}

	/**
	 * Generate Image
	 * template: Sharp Template with bad beat already on it
	 * cardCombo: Array with card1, card2
	 **/
	generateImage(template, beatName, cardCombo){
		// Ensure the folder exists for root and bad beat
		if( !fs.existsSync(config.get('storage.path')) ){
			console.log('Root output directory doesn\'t exist, making it...');
		}

		var beatDirectoryPath = config.get('storage.path') + beatName + '/';
		var cardDirectoryPath = config.get('storage.path') + 'cards/';
		if( !fs.existsSync(beatDirectoryPath) ){
			console.log('Creating output directory for '+beatName);
			fs.mkdirSync(beatDirectoryPath);
		}

			// Generate the card images to place on to the template
		let finalFilePath = beatDirectoryPath + cardCombo[0][0] + '-' + cardCombo[0][1] + '-' + cardCombo[1][0] + '-' + cardCombo[1][1];
		if( !fs.existsSync(finalFilePath+'.png') ){
			console.log('Need to generate: '+finalFilePath);
			let masterTemp = template;
			let cardProcs = [];
			let cardImageTemp = sharp(this.cardPath + 'card-background.svg');
				let cardIdent = 1;
				for( let card in cardCombo ){
					console.log(this.i);
					let suitIcon = '';
					let cardIcon = '';

					// Red
					if( ['heart', 'diamond'].indexOf(cardCombo[card][1]) >= 0 ){
						suitIcon = this.cardPath + 'icon-'+cardCombo[card][1]+'.svg';
						 cardIcon = this.cardPath + 'icn-'+cardCombo[card][0]+'-red.svg';
					}
					// Black
					else {
						suitIcon = this.cardPath + 'icon-'+cardCombo[card][1]+'.svg';
						cardIcon = this.cardPath + 'icn-'+cardCombo[card][0]+'-black.svg';
					}


					let cardImage = cardImageTemp.clone();
					cardProcs.push(new Promise((resolve, reject) => {
						let iz = this.i;
						cardImage.toBuffer().then((buff) => {
							this.ii++;
							console.log('Overlaying suit icon...');
							console.log(this.ii);
							
							sharp(buff).overlayWith(suitIcon, {
								left: config.get('template.cards.suit-icon.left'),
								top: config.get('template.cards.suit-icon.top')
							}).toBuffer().then((buffTwo) => {
								this.iii++;
								console.log('Overlaying card icon...');
								console.log(this.iii+'/'+(this.combinations.length*2));

								sharp(buffTwo).overlayWith(cardIcon, {
									left: config.get('template.cards.card-icon.left'),
									top: config.get('template.cards.card-icon.top')
								}).toBuffer().then((buff) => {
									this.iiii++;
									console.log('About to save... '+this.iiii);

									let cardFilePath = cardDirectoryPath+'card'+iz+'.png';
									sharp(buff).toBuffer().then(cardBuff => {
										console.log('Saved card buffer!');

										resolve(cardBuff);
									});
								});
							});
						});
					}));

					this.i++;
				}

				Promise.all(cardProcs).then((itm) => {
					console.log('Card has been saved, promise triggered, ready to place on to template');
					console.log('Card '+cardIdent);

					masterTemp.toBuffer().then((buff) => {
						this.procCounter++;
						console.log('Loaded both cards as buffers for '+this.procCounter)
						
						sharp(buff).overlayWith(itm[0], {
							left: config.get('template.cards.card-1.left'),
							top: config.get('template.cards.card-1.top')
						}).toBuffer().then(buff => {
							this.procCounterTwo++;
							console.log('Final stage for '+this.procCounterTwo);
						

							sharp(buff).overlayWith(itm[1], {
								left: config.get('template.cards.card-2.left'),
								top: config.get('template.cards.card-2.top')
							}).toFile(finalFilePath+'.png').then((buff) => {
								console.log('Saved finalised template '+finalFilePath);
							});
						});
					});

					cardIdent++;
				});

				console.log(cardProcs);
		}
	}
};